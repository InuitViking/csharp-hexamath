# CSharp HexaMath
[Link to wiki](https://gitlab.com/InuitViking/csharp-hexamath/wikis/Introduction)

This is a (very young) C# hexadecimal mathematical package thingie.
Currently, it only holds below functions:
1. [Convert hexadecimal double to decimal double](https://gitlab.com/InuitViking/csharp-hexamath/wikis/Introduction#hextodouble)
2. [Convert decimal double to hexadecimal double](https://gitlab.com/InuitViking/csharp-hexamath/wikis/Introduction#doubletohex)
3. [Increment a hexadecimal integer by 1](https://gitlab.com/InuitViking/csharp-hexamath/wikis/Introduction#hexincrement)