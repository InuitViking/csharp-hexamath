using System.Security.AccessControl;
using System.Linq;
using System.Collections.Generic;
using System;
namespace HexaMath.Mathematics
{
	public class Mathematics
	{
		/// <summary>
		/// HexIncrement, as the name suggests, increments a hexadecimal number.
		/// More specifically, it increments a hexadecimal integer by one (1).
		/// The parameter must be of type Hexadecimal, and NOT var.Hex; this is for the ease of use to the programmer utilising this method.
		/// The method returns a Hexadecimal object type, and NOT a string.
		/// </summary>
		/// <param name="hex"></param>
		/// <returns>Hexadecimal</returns>
		public static Hexadecimal HexIncrement(Hexadecimal hex){
			// "Table" containing hexadecimal values. Used for the index to return the char value on the represented index.
			char[] hexTable = new char[]{'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
			// hexCharList is used to put the individual parts of the string into chars in a list
			List<char> hexCharList = new List<char>{};
			// hexCharListReverse is the same, but in reverse to "easier do math"
			List<char> hexCharListReverse = new List<char>{};
			// newHexCharList will be our new List containing new values
			List<char> newHexCharList = new List<char>{};

			// hexCharArray to put the things into an array, because it's easier to put in an array, and later in a list
			char[] hexCharArray = hex.Hex.ToCharArray();

			// Will contain the positional index from hexTable depending on the values it contains.
			int pos = 0;

			// Puts the hexCharArray into hexCharList
			for (int i = 0; i < hexCharArray.Length; i++)
			{
				hexCharList.Add(hexCharArray[i]);
			}

			// Reversively puts the hexCharArray into hexCharListReverse.
			// using hexCharArray.Reverse() would result in IEnumerable errors. This musn't happen as we need them indexed
			for (int i = hexCharArray.Length-1; i >= 0; i--)
			{
				hexCharListReverse.Add(hexCharArray[i]);
			}

			// Boolean for checking whether the next number should be incremented or not
			bool shouldNextIncrement = false;

			/*
				What these weird nested statements do, is basically
				increment the hexadecimal number, and check whether
				the next number in the char array should be
				incremented one up or not, (in case the
				number before was an F).

				If the lowest(?) integer is F from the start, it
				should be shifted to a 0 and the next in the row
				should be incremented one up.
				If  the next one to be incremented one up is also
				F, then change it to 0 and increment the next one
				up, and this continues until the there are no more
				numbers.
			*/
			if(hexCharListReverse.Count == 1){
				if(hexCharListReverse[0] == hexTable[15]){
					hexCharListReverse.Add('1');
					hexCharListReverse[0] = hexTable[0];
				}else{
					pos = Array.BinarySearch(hexTable, hexCharListReverse[0]);
					hexCharListReverse[0] = hexTable[pos+1];
				}
			}else{
				if (hexCharListReverse.All(o => o == hexTable[15])){
					hexCharListReverse = hexCharListReverse.Select (x => '0') .ToList();
					hexCharListReverse.Add('1');
				}else{
					int i;
					for (i=0;i < hexCharListReverse.Count; i++)
					{
						pos = Array.BinarySearch(hexTable, hexCharListReverse[i]);
						if(shouldNextIncrement){

							if(hexCharListReverse[i] == hexTable[15]){
								shouldNextIncrement = true;
								hexCharListReverse[i] = hexTable[0];
							}else if(hexCharListReverse[i] != hexTable[15]){
								hexCharListReverse[i] = hexTable[pos+1];
								shouldNextIncrement = false;
								break;
							}
						}else{
							if(hexCharListReverse[i] == hexTable[15]){
								shouldNextIncrement = true;
								hexCharListReverse[i] = hexTable[0];
							}else if(hexCharListReverse[i] != hexTable[15]){
								hexCharListReverse[i] = hexTable[pos+1];
								shouldNextIncrement = false;
								break;
							}
						}
					}
				}
			}

			/*
				Reversively put the contents of hexCharListReverse into
				newHexCharList. (This means they'll be in the right)
				order, because we reverse the reversed list, thus
				irreversing it... Right?
			*/
			for (int i = hexCharListReverse.Count-1; i >= 0; i--)
			{
				newHexCharList.Add(hexCharListReverse[i]);
			}

			// Create a new string out of newHexCHarList
			hex.Hex = new string(newHexCharList.ToArray());
			// Return our incremented hexadecimal
			return hex;
		}// End of HexIncrement
	}
}