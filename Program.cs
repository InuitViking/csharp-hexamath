﻿using System;
using static HexaMath.ConvHex.ConvHex;
using static HexaMath.Mathematics.Mathematics;

namespace HexaMath
{
	class Program
	{
		static void Main(string[] args)
		{
			try
			{
				double dnum = 9104.89;
				Hexadecimal doubleToHex = DoubleToHex(dnum);
				Console.WriteLine("Double \"{0}\" to hex: {1}",dnum,doubleToHex.Hex);
				Console.WriteLine("Hex \"{0}\" to double: {1}",doubleToHex.Hex,HexToDouble(doubleToHex.Hex));
				Hexadecimal hexInt = new Hexadecimal();
				hexInt.Hex = "0";
				Console.WriteLine($"hexInt variable, not incrementet: {hexInt.Hex}");
				hexInt = HexIncrement(hexInt);
				Console.WriteLine($"hexInt variable, incrementet: {hexInt.Hex}");
			}
			catch (System.Exception)
			{
				Console.WriteLine("The program crashed, probably because of invalid numbers being used.");
				Console.WriteLine("See thrown exception below:");
				throw;
			}
		}
	}
}
